Feature: Interpreters management

  Scenario: create a new interpreter and get it
    Given I have a payload for a new interpreter
    When I POST it 1 time to the /interpreters endpoint
    Then I receive a 201 status code
    And I am able to GET the interpreter with the received location header

  Scenario: create an interpreter with an already-existing e-mail address
    Given I have a payload for a new interpreter
    When I POST it 2 times to the /interpreters endpoint
    Then I receive a 422 status code

  Scenario: create an interpreter with an non-existent language
    Given I have a payload for a new interpreter with a non-existent language
    When I POST it 1 time to the /interpreters endpoint
    Then I receive a 404 status code

  Scenario: get a paginated list of interpreters
    Given I have a payload for a new interpreter
    When I POST it 1 time to the /interpreters endpoint
    Then I must be able to paginate 1 pages of 1 interpreters each from the /interpreters endpoint and receive a 200 status code
    Given I have a payload for a new interpreter
    When I POST it 1 time to the /interpreters endpoint
    Then I must be able to paginate 1 page of 2 interpreters each from the /interpreters endpoint and receive a 200 status code
    And I must be able to paginate 2 pages of 1 interpreter each from the /interpreters endpoint and receive a 200 status code

  Scenario: update an interpreter
    Given I have an interpreter
    And I have a payload to update the interpreter
    When I PATCH the payload to the /interpreters/:id endpoint
    Then I receive a 201 status code
    And I am able to GET the interpreter with the received location header

  Scenario: update a non-existent interpreter
    Given I have the ID of a non-existent interpreter
    And I have a payload to update the interpreter
    When I PATCH the payload to the /interpreters/:id endpoint
    Then I receive a 404 status code

  # Here we don't have a payload to update the interpreter so the test will use the same one that was used to create the
  # interpreter at first, which means that the e-mail address will be the same.
  Scenario: update an interpreter with an already-existing e-mail address
    Given I have an interpreter
    When I PATCH the payload to the /interpreters/:id endpoint
    Then I receive a 422 status code

  Scenario: delete an interpreter
    Given I have an interpreter
    When I DELETE it with the /interpreters/:id endpoint
    Then I receive a 204 status code
    And I am unable to GET the interpreter with the received location header

  Scenario: delete an non-existent interpreter
    Given I have the ID of a non-existent interpreter
    When I DELETE it with the /interpreters/:id endpoint
    Then I receive a 404 status code