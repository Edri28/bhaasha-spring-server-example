Feature: Languages management

  Scenario: create a new language and get it
    Given I have a payload for a new language
    When I POST it 1 time to the /languages endpoint
    Then I receive a 201 status code
    And I am able to GET the language with the received location header

  Scenario: create a language with an already-existing name
    Given I have a payload for a new language
    When I POST it 2 times to the /languages endpoint
    Then I receive a 422 status code

  Scenario: get a list of languages
    Given I have a payload for a new language
    When I POST it 1 time to the /languages endpoint
    Then I must be able to GET the languages from the /languages endpoint and receive a 200 status code
