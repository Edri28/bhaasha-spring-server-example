package io.bhaasha.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.bhaasha.ApiException;
import io.bhaasha.api.InterpretersApi;
import io.bhaasha.entities.Interpreter;
import io.bhaasha.entities.NewInterpreter;
import io.bhaasha.helpers.Environment;
import io.bhaasha.ApiResponse;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneOffset;

import java.util.*;

import static org.junit.Assert.*;

public class InterpretersSteps {

    // Injected objects shared with other scenarios.
    private Environment environment;

    private InterpretersApi interpretersApi;
    private NewInterpreter newInterpreter;
    private Long interpreterId;

    // Inject the environment in the constructor.
    public InterpretersSteps(Environment environment) {
        this.environment = environment;
        this.interpretersApi = this.environment.getInterpreterApi();
    }

    @Given("^I have a payload for a new interpreter$")
    public void iHaveAPayloadForANewInterpreter() {
        UUID uuid = UUID.randomUUID();
        newInterpreter = new NewInterpreter();
        newInterpreter.setFirstName("Firstname");
        newInterpreter.setLastName("Lastname");
        newInterpreter.setBirthDate(OffsetDateTime.of(1990, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC));
        newInterpreter.setEmail(new StringBuilder("email_").append(uuid).append("@bhaasha.ch").toString());
    }

    @Given("^I have a payload to update the interpreter$")
    public void iHaveAPayloadToUpdateTheInterpreter() throws Throwable {
        UUID uuid = UUID.randomUUID();
        newInterpreter = new NewInterpreter();
        newInterpreter.setFirstName("UpdatedFirstname");
        newInterpreter.setLastName("UpdatedLastname");
        newInterpreter.setBirthDate(OffsetDateTime.of(1991, 2, 3, 0, 0, 0, 0, ZoneOffset.UTC));
        newInterpreter.setEmail(new StringBuilder("email_").append(uuid).append("@bhaasha.ch").toString());
    }

    @Given("^I have a payload for a new interpreter with a non-existent language$")
    public void iHaveAPayloadForANewInterpreterWithANonExistentLanguage() throws Throwable {
        iHaveAPayloadForANewInterpreter();
        newInterpreter.setLanguagesIds(new ArrayList<Long>(Arrays.asList(-1L)));
    }

    @Given("^I have an interpreter$")
    public void iHaveAnInterpreter() {
        iHaveAPayloadForANewInterpreter();
        iPostItToTheInterpretersEndpoint(1);
    }

    @Given("^I have the ID of a non-existent interpreter$")
    public void iHaveTheIDOfANonExistentInterpreter() throws Throwable {
        interpreterId = -1L;
    }

    @When("^I POST it (\\d+) times? to the /interpreters endpoint$")
    public void iPostItToTheInterpretersEndpoint(int numberOfPosts) {
        try {
            for (int i = 0; i < numberOfPosts; ++i) {
                environment.setLastApiResponse(interpretersApi.addInterpreterWithHttpInfo(newInterpreter));
            }
            environment.setLastApiException(null);
            environment.setLastApiCallThrewException(false);
        } catch (ApiException e) {
            environment.setLastApiResponse(null);
            environment.setLastApiException(e);
            environment.setLastApiCallThrewException(true);
        }
    }

    @When("^I PATCH the payload to the /interpreters/:id endpoint$")
    public void iPatchThePayloadToTheInterpretersIdEndpoint() throws Throwable {
        // Get the ID of the interpreter to update.
        if (interpreterId == null) {
            interpreterId = environment.getIdFromLastApiResponse();
        }

        try {
            environment.setLastApiResponse(interpretersApi.updateInterpreterWithHttpInfo(interpreterId, newInterpreter));
            environment.setLastApiException(null);
            environment.setLastApiCallThrewException(false);
        } catch (ApiException e) {
            environment.setLastApiResponse(null);
            environment.setLastApiException(e);
            environment.setLastApiCallThrewException(true);
        }
    }

    @When("^I DELETE it with the /interpreters/:id endpoint$")
    public void iDeleteItWithTheInterpretersIdEndpoint() throws Throwable {
        // Get the ID of the interpreter to delete.
        if (interpreterId == null) {
            interpreterId = environment.getIdFromLastApiResponse();
        }

        try {
            environment.setLastApiResponse(interpretersApi.deleteInterpreterWithHttpInfo(interpreterId));
            environment.setLastApiException(null);
            environment.setLastApiCallThrewException(false);
        } catch (ApiException e) {
            environment.setLastApiResponse(null);
            environment.setLastApiException(e);
            environment.setLastApiCallThrewException(true);
        }
    }

    @Then("^I am able to GET the interpreter with the received location header$")
    public void iAmAbleToGetTheInterpreterFromTheLocationHeader() {
        if (environment.hasLastApiCallThrewException()) {
            fail("The last API call threw an exception.");
        } else {
            // Get the new-created interpreter's ID.
            Long interpreterId = environment.getIdFromLastApiResponse();

            try {
                // Get the new-created interpreter's data by its ID, and check that they match with the inserted interpreter.
                Interpreter interpreter = interpretersApi.getInterpreterById(interpreterId);

                assertEquals(interpreter.getFirstName(), newInterpreter.getFirstName());
                assertEquals(interpreter.getLastName(), newInterpreter.getLastName());
                assertEquals(interpreter.getBirthDate(), newInterpreter.getBirthDate());
                assertEquals(interpreter.getEmail(), newInterpreter.getEmail());
            } catch (ApiException e) {
                fail("Unable to get the new-created interpreter: " + e.getMessage());
            }
        }
    }

    @Then("^I must be able to paginate (\\d+) pages? of (\\d+) interpreters? each from the /interpreters endpoint and receive a (\\d+) status code$")
    public void iMustBeAbleToPaginatePagesOfInterpretersEachFromTheInterpretersEndpoint(int numberOfPages, int sizeOfPages, int code) {
        if (environment.hasLastApiCallThrewException()) {
            fail("The last API call threw an exception.");
        } else {
            try {
                for (int i = 0; i < numberOfPages; ++i) {
                    ApiResponse<List<Interpreter>> apiResponse = interpretersApi.getInterpretersWithHttpInfo(i, sizeOfPages);
                    assertEquals(apiResponse.getStatusCode(), code);
                    assertFalse(apiResponse.getData().isEmpty());
                }
            } catch (ApiException e) {
                fail("Unable to get the interpreters: " + e.getMessage());
            }
        }
    }

    @Then("^I am unable to GET the interpreter with the received location header$")
    public void iAmUnableToGETTheInterpreterWithTheReceivedLocationHeader() throws Throwable {
        if (environment.hasLastApiCallThrewException()) {
            fail("The last API call threw an exception.");
        } else {
            try {
                // Get the new-created interpreter's data by its ID, and check that they match with the inserted interpreter.
                interpretersApi.getInterpreterByIdWithHttpInfo(interpreterId);
                fail("It was possible to get the deleted interpreter.");
            } catch (ApiException e) {
                assertEquals(e.getCode(), 404);
            }
        }
    }
}
