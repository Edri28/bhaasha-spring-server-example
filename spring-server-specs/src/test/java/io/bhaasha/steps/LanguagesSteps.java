package io.bhaasha.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.bhaasha.ApiException;
import io.bhaasha.ApiResponse;
import io.bhaasha.api.LanguagesApi;
import io.bhaasha.entities.Language;
import io.bhaasha.entities.NewLanguage;
import io.bhaasha.helpers.Environment;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class LanguagesSteps {

    // Injected objects shared with other scenarios.
    private Environment environment;

    private LanguagesApi languagesApi;
    private NewLanguage newLanguage;

    // Inject the environment in the constructor.
    public LanguagesSteps(Environment environment) {
        this.environment = environment;
        this.languagesApi = this.environment.getLanguagesApi();
    }

    @Given("^I have a payload for a new language")
    public void iHaveAPayloadForANewLanguage() {
        UUID uuid = UUID.randomUUID();
        newLanguage = new NewLanguage();
        newLanguage.setName(new StringBuilder("name_").append(uuid).toString());
        newLanguage.setFlagUrl("url");
    }

    @When("^I POST it (\\d+) times? to the /languages endpoint$")
    public void iPostItTimeToTheLanguagesEndpoint(int numberOfPosts) {
        try {
            for (int i = 0; i < numberOfPosts; ++i) {
                environment.setLastApiResponse(languagesApi.addLanguageWithHttpInfo(newLanguage));
            }
            environment.setLastApiException(null);
            environment.setLastApiCallThrewException(false);
        } catch (ApiException e) {
            environment.setLastApiResponse(null);
            environment.setLastApiException(e);
            environment.setLastApiCallThrewException(true);
        }
    }

    @Then("^I am able to GET the language with the received location header$")
    public void iAmAbleToGetTheLanguageWithTheReceivedLocationHeader() {
        if (environment.hasLastApiCallThrewException()) {
            fail("The last API call threw an exception.");
        } else {
            // Get the new-created interpreter's ID.
            Long languageId = environment.getIdFromLastApiResponse();

            try {
                // Get the new-created interpreter's data by its ID, and check that they match with the inserted interpreter.
                Language language = languagesApi.getLanguageById(languageId);

                assertEquals(language.getName(), newLanguage.getName());
                assertEquals(language.getFlagUrl(), newLanguage.getFlagUrl());
            } catch (ApiException e) {
                fail("Unable to get the new-created interpreter: " + e.getMessage());
            }
        }
    }

    @Then("^I must be able to GET the languages from the /languages endpoint and receive a (\\d+) status code$")
    public void iMustBeAbleToGetTheLanguagesFromTheLanguagesEndpointAndReceiveAStatusCode(int code) {
        if (environment.hasLastApiCallThrewException()) {
            fail("The last API call threw an exception.");
        } else {
            try {
                ApiResponse<List<Language>> apiResponse = languagesApi.getLanguagesWithHttpInfo();
                assertEquals(apiResponse.getStatusCode(), code);
                assertFalse(apiResponse.getData().isEmpty());
            } catch (ApiException e) {
                fail("Unable to get the languages: " + e.getMessage());
            }
        }
    }
}
