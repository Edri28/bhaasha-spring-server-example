package io.bhaasha.steps;

import cucumber.api.java.en.Then;
import io.bhaasha.helpers.Environment;

import static org.junit.Assert.assertEquals;

public class SharedSteps {

    // Injected objects shared with other scenarios.
    private Environment environment;

    // Inject the environment in the constructor.
    public SharedSteps(Environment environment) {
        this.environment = environment;
    }


    @Then("^I receive a (\\d+) status code$")
    public void iReceiveAStatusCode(int code) {
        int codeToCheck;

        if (environment.hasLastApiCallThrewException()) {
            codeToCheck = environment.getLastApiException().getCode();
        } else {
            codeToCheck = environment.getLastApiResponse().getStatusCode();
        }

        assertEquals(code, codeToCheck);
    }
}
