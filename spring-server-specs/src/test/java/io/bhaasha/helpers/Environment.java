package io.bhaasha.helpers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import io.bhaasha.ApiException;
import io.bhaasha.ApiResponse;
import io.bhaasha.api.InterpretersApi;
import io.bhaasha.api.LanguagesApi;

/**
 * The same instance of this class is injected in each steps file through its constructor, so they can share data.
 */
public class Environment {

    private InterpretersApi interpreterApi = new InterpretersApi();
    private LanguagesApi languagesApi = new LanguagesApi();

    private ApiResponse lastApiResponse;
    private ApiException lastApiException;
    private boolean lastApiCallThrewException;

    public Environment() throws IOException {
        Properties properties = new Properties();
        properties.load(this.getClass().getClassLoader().getResourceAsStream("environment.properties"));
        String url = properties.getProperty("io.bhaasha.server.url");
        interpreterApi.getApiClient().setBasePath(url);
        languagesApi.getApiClient().setBasePath(url);
    }

    public Long getIdFromLastApiResponse() {
        // Get the "location" header from the last API response, which contains a link to get the new interpreter and
        // which must be formatted as the following: "/api/interpreter/{interpreterId}".
        String location = ((ArrayList<String>)lastApiResponse.getHeaders().get("Location")).get(0);
        // Get and return the new-created interpreter's ID.
        return Long.parseLong(location.substring(location.lastIndexOf('/') + 1));
    }

    public InterpretersApi getInterpreterApi() {
        return interpreterApi;
    }

    public LanguagesApi getLanguagesApi() {
        return languagesApi;
    }

    public ApiResponse getLastApiResponse() {
        return lastApiResponse;
    }

    public void setLastApiResponse(ApiResponse lastApiResponse) {
        this.lastApiResponse = lastApiResponse;
    }

    public ApiException getLastApiException() {
        return lastApiException;
    }

    public void setLastApiException(ApiException lastApiException) {
        this.lastApiException = lastApiException;
    }

    public boolean hasLastApiCallThrewException() {
        return lastApiCallThrewException;
    }

    public void setLastApiCallThrewException(boolean lastApiCallThrewException) {
        this.lastApiCallThrewException = lastApiCallThrewException;
    }
}
