package io.bhaasha.repositories;

import io.bhaasha.entities.LanguageEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

public interface LanguageRepository extends PagingAndSortingRepository<LanguageEntity, Long> {

    LanguageEntity findByName(@Param(value = "name") String name);
}
