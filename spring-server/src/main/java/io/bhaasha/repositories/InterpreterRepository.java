package io.bhaasha.repositories;

import io.bhaasha.entities.InterpreterEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

public interface InterpreterRepository extends PagingAndSortingRepository<InterpreterEntity, Long> {

    InterpreterEntity findByEmail(@Param(value = "email") String email);
}
