package io.bhaasha.entities;

import org.threeten.bp.OffsetDateTime;

import javax.persistence.*;

@Entity
public class LanguageEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private OffsetDateTime creationDate;

  @Column(unique = true, nullable = false)
  private String name;

  private String flagUrl;

  public Long getId() {
    return id;
  }

  public OffsetDateTime getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(OffsetDateTime creationDate) {
    this.creationDate = creationDate;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFlagUrl() {
    return flagUrl;
  }

  public void setFlagUrl(String flagUrl) {
    this.flagUrl = flagUrl;
  }
}

