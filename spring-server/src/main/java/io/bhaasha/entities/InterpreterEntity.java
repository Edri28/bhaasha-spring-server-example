package io.bhaasha.entities;

import org.threeten.bp.OffsetDateTime;

import java.util.List;

import javax.persistence.*;

@Entity
public class InterpreterEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private OffsetDateTime creationDate;

  @Column(nullable = false)
  private String firstName;

  @Column(nullable = false)
  private String lastName;

  private OffsetDateTime birthDate;

  @Column(unique = true, nullable = false)
  private String email;

  @ManyToMany
  private List<LanguageEntity> languages;

  public Long getId() {
    return id;
  }

  public OffsetDateTime getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(OffsetDateTime creationDate) {
    this.creationDate = creationDate;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public OffsetDateTime getBirthDate() {
    return birthDate;
  }

  public void setBirthDate(OffsetDateTime birthDate) {
    this.birthDate = birthDate;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public List<LanguageEntity> getLanguages() {
    return languages;
  }

  public void setLanguages(List<LanguageEntity> languages) {
    this.languages = languages;
  }
}

