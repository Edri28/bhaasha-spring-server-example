package io.bhaasha.api.utils;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-28T14:21:07.251Z")

public class ApiException extends RuntimeException{
    private int code;
    public ApiException (int code, String msg) {
        super(msg);
        this.code = code;
    }
}
