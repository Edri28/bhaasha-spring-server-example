package io.bhaasha.api.endpoints;

import io.bhaasha.api.LanguagesApi;
import io.bhaasha.api.services.UrisService;
import io.bhaasha.entities.LanguageEntity;
import io.bhaasha.model.Language;
import io.bhaasha.model.NewLanguage;
import io.bhaasha.repositories.LanguageRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.threeten.bp.OffsetDateTime;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-28T14:21:07.251Z")

@Controller
@Api(tags = {"Languages"})
public class LanguagesApiController implements LanguagesApi {

    @Autowired
    LanguageRepository languageRepository;

    @Override
    public ResponseEntity<Void> addLanguage(@ApiParam(value = "Language object that needs to be added", required = true) @Valid @RequestBody NewLanguage language) {
        // Check if there already is an existing language with the given name, and return an error if so.
        LanguageEntity existingLanguage = languageRepository.findByName(language.getName());

        if (existingLanguage != null) {
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        // Deserialize the received language object and turn it into an entity so we can write it in the database.
        LanguageEntity languageEntity = newLanguageToLanguageEntity(language);
        // Save the new language in the database.
        LanguageEntity newLanguage = languageRepository.save(languageEntity);

        try {
            // Return a http response with a link to access the new-created entity in the "location" header.
            return ResponseEntity
                    .created(new URI(UrisService.getLanguageUri(newLanguage))).build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Language> getLanguageById(@ApiParam(value = "ID of language to return", required = true) @PathVariable("languageId") Long languageId) {
        Optional<LanguageEntity> existingLanguageEntity = languageRepository.findById(languageId);

        // The language must exist in the database.
        if (!existingLanguageEntity.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        // Serialize the entity into a model.
        Language language = languageEntityToLanguage(existingLanguageEntity.get());

        return ResponseEntity
                .ok(language);
    }

    @Override
    public ResponseEntity<List<Language>> getLanguages() {
        List<Language> languages = new ArrayList<>();

        // Convert each entity into its corresponding model in order to send them back as JSON.
        for (LanguageEntity languageEntity : languageRepository.findAll()) {
            languages.add(languageEntityToLanguage(languageEntity));
        }

        return ResponseEntity
                .ok()
                .header("X-Total-Count", Integer.toString(languages.size()))
                .body(languages);
    }

    /**
     * Convert a language entity into a language model.
     * @param languageEntity the language entity (coming from the repository) to convert
     * @return the language model
     */
    private Language languageEntityToLanguage(LanguageEntity languageEntity) {
        Language language = new Language();
        language.setId(languageEntity.getId());
        language.setCreationDate(languageEntity.getCreationDate());
        language.setName(languageEntity.getName());
        language.setFlagUrl(languageEntity.getFlagUrl());

        return language;
    }

    /**
     * Convert a new (does not contain the ID and creation date's fields) language model into an language entity.
     * @param newLanguage the language model to convert
     * @return the language entity
     */
    private LanguageEntity newLanguageToLanguageEntity(NewLanguage newLanguage) {
        LanguageEntity languageEntity = new LanguageEntity();
        languageEntity.setCreationDate(OffsetDateTime.now());
        languageEntity.setName(newLanguage.getName());
        languageEntity.setFlagUrl(newLanguage.getFlagUrl());

        return languageEntity;
    }
}
