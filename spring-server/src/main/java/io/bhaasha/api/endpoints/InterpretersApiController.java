package io.bhaasha.api.endpoints;

import io.bhaasha.api.InterpretersApi;
import io.bhaasha.api.services.UrisService;
import io.bhaasha.api.utils.ApiException;
import io.bhaasha.entities.InterpreterEntity;
import io.bhaasha.entities.LanguageEntity;
import io.bhaasha.model.Interpreter;
import io.bhaasha.model.Language;
import io.bhaasha.model.NewInterpreter;
import io.bhaasha.repositories.InterpreterRepository;
import io.bhaasha.repositories.LanguageRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.threeten.bp.OffsetDateTime;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-05-28T14:21:07.251Z")

@Controller
@Api(tags = {"Interpreters"}) // Used to avoid the controller beeing duplicated in the Swagger UI.
public class InterpretersApiController implements InterpretersApi {

    @Autowired
    InterpreterRepository interpreterRepository;

    @Autowired
    LanguageRepository languageRepository;

    @Override
    public ResponseEntity<Void> addInterpreter(@ApiParam(value = "Interpreter object that needs to be added", required = true) @Valid @RequestBody NewInterpreter interpreter) {
        // Check if there already is an existing interpreter with the given e-mail address, and return an error if so.
        InterpreterEntity existingInterpreter = interpreterRepository.findByEmail(interpreter.getEmail());

        if (existingInterpreter != null) {
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }

        // Deserialize the received interpreter object and turn it into an entity so we can write it in the database.
        InterpreterEntity interpreterEntity;

        try {
            interpreterEntity = newInterpreterToIntepretedEntity(interpreter);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        // Save the new interpreter in the database.
        InterpreterEntity newInterpreter = interpreterRepository.save(interpreterEntity);

        try {
            // Return a http response with a link to access the new-created entity in the "location" header.
            return ResponseEntity
                    .created(new URI(UrisService.getInterpreterUri(newInterpreter))).build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> deleteInterpreter(@ApiParam(value = "ID of interpreter to delete", required = true) @PathVariable("interpreterId") Long interpreterId) {
        Optional<InterpreterEntity> existingInterpreterEntity = interpreterRepository.findById(interpreterId);

        // The interpreter to delete must exist in the database.
        if (!existingInterpreterEntity.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        // Delete the interpreter from the database.
        interpreterRepository.delete(existingInterpreterEntity.get());

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<Interpreter> getInterpreterById(@ApiParam(value = "ID of interpreter to return", required = true) @PathVariable("interpreterId") Long interpreterId) {
        Optional<InterpreterEntity> existingInterpreterEntity = interpreterRepository.findById(interpreterId);

        // The interpreter must exist in the database.
        if (!existingInterpreterEntity.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        // Serialize the entity into a model.
        Interpreter readableInterpreter = interpreterEntityToInterpreter(existingInterpreterEntity.get());

        return ResponseEntity
                .ok(readableInterpreter);
    }

    @Override
    public ResponseEntity<List<Interpreter>> getInterpreters(@ApiParam(value = "the page number for the pagination (starting from 0)") @Valid @RequestParam(value = "page", required = false) Integer page, @ApiParam(value = "the page size for the pagination") @Valid @RequestParam(value = "size", required = false) Integer size) {
        List<Interpreter> interpreters = new ArrayList<>();
        Iterable<InterpreterEntity> interpretersFromDb;

        // Get the interpreters entities from the database.
        if (page != null && size != null) {
            // Manage pagination if the user gave the right pagination's parameters.
            Pageable pageable = PageRequest.of(page, size, Sort.by("lastName").and(Sort.by("firstName")));
            interpretersFromDb = interpreterRepository.findAll(pageable);
        } else {
            interpretersFromDb = interpreterRepository.findAll();
        }

        // Convert each entity into its corresponding model in order to send them back as JSON.
        for (InterpreterEntity interpreterEntity : interpretersFromDb) {
            interpreters.add(interpreterEntityToInterpreter(interpreterEntity));
        }

        return ResponseEntity
                .ok()
                .header("X-Total-Count", Integer.toString(interpreters.size()))
                .body(interpreters);
    }

    @Override
    public ResponseEntity<Void> updateInterpreter(@ApiParam(value = "ID of interpreter to update", required = true) @PathVariable("interpreterId") Long interpreterId, @ApiParam(value = "Interpreter object that needs to be updated", required = true) @Valid @RequestBody NewInterpreter interpreter) {
        Optional<InterpreterEntity> existingInterpreterEntity = interpreterRepository.findById(interpreterId);

        // The interpreter to update must exist in the database.
        if (!existingInterpreterEntity.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        InterpreterEntity interpreterEntity = existingInterpreterEntity.get();

        // Update each possibly updated field.
        if (interpreter.getFirstName() != null) {
            interpreterEntity.setFirstName(interpreter.getFirstName());
        }

        if (interpreter.getLastName() != null) {
            interpreterEntity.setLastName(interpreter.getLastName());
        }

        if (interpreter.getBirthDate() != null) {
            interpreterEntity.setBirthDate(interpreter.getBirthDate());
        }

        if (interpreter.getEmail() != null) {
            // Check if there already is an existing user with the given e-mail address, and return an error if so.
            String email = interpreter.getEmail();
            InterpreterEntity existingInterpreter = interpreterRepository.findByEmail(email);

            if (existingInterpreter != null) {
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }

            interpreterEntity.setEmail(interpreter.getEmail());
        }

        if (interpreter.getLanguagesIds() != null && !interpreter.getLanguagesIds().isEmpty()) {
            try {
                interpreterEntity.setLanguages(languagesIdsToLanguageEntities(interpreter.getLanguagesIds()));
            } catch (Exception e) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }

        // Save the modifications in the database.
        InterpreterEntity updatableInterpreter = interpreterRepository.save(interpreterEntity);

        try {
            // Return a http response with a link to access the updated entity in the "location" header.
            return ResponseEntity
                    .created(new URI(UrisService.getInterpreterUri(updatableInterpreter))).build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * Convert a list of languages IDs to a list of languages entities.
     * @param languagesIds the list of languages IDs to convert
     * @return the list of languages entities
     */
    private List<LanguageEntity> languagesIdsToLanguageEntities(List<Long> languagesIds) throws Exception {
        List<LanguageEntity> languages = new ArrayList<>();

        // Convert the languages IDs into languages entities.
        for (Long languageId : languagesIds) {
            Optional<LanguageEntity> languageEntity = languageRepository.findById(languageId);

            if (languageEntity.isPresent()) {
                languages.add(languageEntity.get());
            } else {
                throw new Exception("Language not found");
            }
        }

        return languages;
    }

    /**
     * Convert a language entity into a language model.
     * @param languageEntity the language entity (coming from the repository) to convert
     * @return the language model
     */
    private Language languageEntityToLanguage(LanguageEntity languageEntity) {
        Language language = new Language();
        language.setId(languageEntity.getId());
        language.setName(languageEntity.getName());
        language.setFlagUrl(languageEntity.getFlagUrl());

        return language;
    }

    /**
     * Convert an interpreter entity into an interpreter model.
     * @param interpreterEntity the interpreter entity (coming from the repository) to convert
     * @return the interpreter model
     */
    private Interpreter interpreterEntityToInterpreter(InterpreterEntity interpreterEntity) {
        Interpreter interpreter = new Interpreter();
        interpreter.setId(interpreterEntity.getId());
        interpreter.setCreationDate(interpreterEntity.getCreationDate());
        interpreter.setFirstName(interpreterEntity.getFirstName());
        interpreter.setLastName(interpreterEntity.getLastName());
        interpreter.setBirthDate(interpreterEntity.getBirthDate());
        interpreter.setEmail(interpreterEntity.getEmail());

        List<Language> languages = new ArrayList<>();

        // Also convert the languages entities of the interpreter to models.
        for (LanguageEntity languageEntity : interpreterEntity.getLanguages()) {
            languages.add(languageEntityToLanguage(languageEntity));
        }

        interpreter.setLanguages(languages);

        return interpreter;
    }

    /**
     * Convert a new (does not contain the ID and creation date's fields) interpreter model into an interpreter entity.
     * @param writableInterpreter the interpreter model to convert
     * @return the interpreter entity
     */
    private InterpreterEntity newInterpreterToIntepretedEntity(NewInterpreter newInterpreter) throws Exception {
        InterpreterEntity interpreterEntity = new InterpreterEntity();
        interpreterEntity.setCreationDate(OffsetDateTime.now());
        interpreterEntity.setFirstName(newInterpreter.getFirstName());
        interpreterEntity.setLastName(newInterpreter.getLastName());
        interpreterEntity.setBirthDate(newInterpreter.getBirthDate());
        interpreterEntity.setEmail(newInterpreter.getEmail());
        interpreterEntity.setLanguages(languagesIdsToLanguageEntities(newInterpreter.getLanguagesIds()));

        return interpreterEntity;
    }
}
