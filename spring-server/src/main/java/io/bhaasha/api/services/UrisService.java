package io.bhaasha.api.services;

import io.bhaasha.entities.InterpreterEntity;
import io.bhaasha.entities.LanguageEntity;

public class UrisService {
    private final static String baseUrl = "/api/v4";

    public static String getInterpreterUri(InterpreterEntity interpreter) {
        return baseUrl + "/interpreters/" + interpreter.getId();
    }

    public static String getLanguageUri(LanguageEntity language) {
        return baseUrl + "/languages/" + language.getId();
    }
}
