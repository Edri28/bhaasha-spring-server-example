# Swagger generated test server
This server was generated with the [swagger-codegen](https://github.com/swagger-api/swagger-codegen) project.

This is an example of building a Swagger-enabled server in Java using the SpringBoot framework.  

The underlying library integrating Swagger in SpringBoot is [Springfox](https://github.com/springfox/springfox).

## Information
We are using a top-down strategy for this backend and the tests, which means that:

1. We specify the [Swagger specifications of the API](https://swagger.io/docs/specification/2-0) in a yaml file (which will also be used to generate the documentation of the endpoints).

2. We generate Java interfaces and models (in the "target/generated-sources/swagger/src/main/java" folder) with the Swagger codegen plugin, based on the yaml spec file.

3. Finally we implement code (controllers and entities) based on the generated interfaces and models.

This project contains two sub-projects:

1. **spring-server**, which contains the backend server of the application.
    - The [Swagger's API specs](https://swagger.io/docs/specification/2-0) are located in the "src/main/resources/api-spec.yaml" file.
    - You can change the default port value in "src/main/resources/application.properties".
    - Build the project with `mvn clean install`, then start your server as a simple java application by running the "target/spring-server-1.0.0.jar" file.

2. **spring-server-specs**, which contains all the [Cucumber](https://cucumber.io/)'s BDD tests of the application.
    - The [Swagger's API specs](https://swagger.io/docs/specification/2-0) are also located in the "src/main/resources/api-spec.yaml" file of the project.
    - Run the tests with `mvn clean test`.

When you update the API specs, be careful to update the **two** yaml files.

The database used in this project is [H2](https://www.h2database.com/html/main.html), an in-memory SQL-based database very useful for the tests. Each time you stop the server, the database will thus be cleaned.

## Access the API documentation (Swagger UI)
In order to access the API documentation, you must run the server and then access the [http://localhost:8080/api/v4/](http://localhost:8080/api/v4/) URL. You can then test each endpoint by clicking the "Try it out" button, filling the optional fields, and finally clicking the "Execute" button.  
![Test an endpoint](img/try-endpoint.png)

# Spring Data REST
This project uses Spring Data REST, which is a part of the Spring Data project that makes it easy to build REST web services that connect to Spring Data repositories.
## Repositories
A repository is an interface that will easily allow one to perform CRUD operations involving entities on the database. At runtime, Spring Data REST will create an implementation of this interface automatically. You can find examples and tutorials [here](https://www.baeldung.com/spring-data-rest-intro) and [here](https://spring.io/guides/gs/accessing-data-rest/).

You can use them to directly generate a REST API, but in this project we are using them in the controllers, which means we don't need the `@RepositoryRestResource` annotation used in the above examples.

In the project, the repositories are located in the "src/main/java/io/bhaasha/repositories" folder. The following example shows the repository linked to the interpreter entity. Basic CRUD methods will be generated as well as the `findByEmail` custom query, which is used to find an interpreter by his e-mail address:  
![repository example](img/repository-example.png)
Regarding custom queries, all you have to do is to ensure that the methods names combine the property names of an entity object and the [supported keywords](https://docs.spring.io/spring-data/data-jpa/docs/current/reference/html/#jpa.query-methods.query-creation). You can also link a method to a SQL query with the [@Query annotation](https://www.baeldung.com/spring-data-jpa-query).

The repository is then used in the controllers thanks to the dependency injection driven by the `@Autowired` annotation.  
![dependency injection of a repository](img/repository-autowired.png)

Finally, you can use the automatically generated CRUD methods (`save`, `findAll`, `findById`, `delete`, ...) as well as the custom queries defined in the repository.  
![repository usage](img/repository-usage.png)

## Add an endpoint
### Add an endpoint in an already-existing controller with already-existing entities
In this example we want to add an endpoint to POST a new language. We will thus add this endpoint in the languages section, which means that we are going to add a new method in the languages controller of the server (LanguagesApiController.java):

1. Add the endpoint in the API specs files ("api-spec.yaml" in **both** projects) by following the [documentation](https://swagger.io/docs/specification/2-0/basic-structure/). You can use the [Swagger editor](http://editor.swagger.io/#) to ensure your syntax is valid. In order to mark your endpoint as an endpoint of an existing controller, you must use the tag of the controller in the `tags` attribute (e.g. "Languages" for the languages controller).  
![Swagger POST language](img/swagger-post-languages.png)

2. Run `mvn clean install` in the backend to generate the sources of the API in the "target/generated-sources/swagger/src/main/java" folder, based on the yaml file. At the end of the process the build should fail, because you still need to implement the new-generated method that represents your new endpoint.  
![Build failure](img/mvn-build-failure.png)

3. In the server code, go in the generated controller interface (e.g. "target/generated-sources/swagger/src/main/java/io/bhaasha/api/LanguagesApi.java") and copy the signature of the new-generated method (e.g. "addLanguage(...)"). It is important to copy the whole line with the annotations.  
![Copy the new-generated method](img/new-generated-method.png)

4. Go in the sources of the implemented controller (e.g. "src/main/java/io/bhaasha/api/endpoints/LanguagesApiController.java") and paste the copied method as an overrided method of the controller. It is important here to paste the method from the generated sources and not automatically generate the method signature with your IDE, since the important annotations (e.g. `@ApiParam`) will not be generated!  
![Paste the method from the generated sources](img/paste-method-from-generated-sources.png)

5. Implement the method. If necessary, update the entities (e.g. "src/main/io/bhaasha/entities/LanguageEntity.java"), repositories (e.g. "src/main/io/bhaasha/repositories/LanguageRepository.java") and services of your data.  
![Implementation of the new method](img/new-method-implementation.png)

6. Run `mvn clean install` again. This time, the build process must succeed.  
![Build success](img/build-success.png)

7. Restart your server and try to access the endpoint from the [generated documentation](http://localhost:8080/api/v4/).  
![New endpoint in the documentation](img/new-endpoint-in-doc.png)

8. You successfully implemented a new endpoint by using a top-down strategy. Now don't forget to write the BDD tests!

### Add an endpoint in a new controller
Here we are going to add a new example controller (ExamplesApiController.java):

1. Generate the new controller's interface by following the first 2 points of the last chapter. Here, the build must not fail since we do not have an implementation of the controller yet.  
![Swagger example controller](img/swagger-example-controller.png)  
![Swagger example controller](img/build-success.png)

2. Create a new controller in the "io/bhaasha/api/endpoints" package by . This controller must be preceded by a `@Controller` annocation and must implements the generated interface.  
In order to avoid this controller being duplicated in the Swagger UI, you must add the following annotation before the declaration of the controller: `@Api(tags = {"ControllerName"})`, where `ControllerName` is the name of the controller in the UI.  
![Swagger example controller](img/example-api-controller.png)

3. Eventually add new entities and repositories in the server.

4. Follow the explanations of the last chapter from point 3.

## Add BDD tests with Cucumber for new endpoints
In this example we are going to add tests for the languages controller:

1. Check that the API specs file of the test project is up-to-date with the one located in the backend server project.

2. Run `mvn clean install` in the *spring-server-specs* project to generate the interfaces and models. In your IDE, if you have `Cannot resolve symbol` errors, you have to mark the "target/generated-sources/swagger" folder as generated source root (in IntelliJ, right-click on the folder, then select "Mark Directory as" > "Generated Sources Root").

3. Write the scenarios linked to the endpoint in the feature file (e.g. "src/test/resources/scenarios/languages.feature" - if necessary, create it). The emphasized lines are the ones whose tests will need to be written. The other ones are already written in one of the existing steps files.  
![Cucumber features](img/cucumber-feature.png)

4. If necessary create a new steps file (e.g. "src/test/java/io/bhaasha/steps/LanguagesSteps.java") based on the existing ones. In this steps file you can inject the `Environment` object (by passing it to the constructor) to share the same data between several steps files (the environment acts like a singleton: there will always be only *one* instance of the environment at the same time, so each time a step file updates a variable of the environment, all the steps files will see the update).  
![Steps file constructor](img/steps-file-constructor.png)

5. Create a function for each step of the scenarios in the steps file. Don't forget to add the `@Given`, `@When` and `@Then` annotations if you create them manually.  
In IntelliJ, you can automatically create the steps by going in the feature file, clicking on a emphasized line, and clicking on the appearing bulb.  
![Create the steps from the feature file in IntelliJ](img/create-steps-from-feature-file.png)  
If you have a step that is used in several features, you can create it in a global step file (in the project we have a "SharedStep.java" file, which is used for that).

6. Code your tests inside the previously-created steps.  
![Example of a step implementation](img/step-implementation.png)

7. Check that your server is running, then run `mvn clean test` to execute your tests!  
![Tests results](img/tests-results.png)